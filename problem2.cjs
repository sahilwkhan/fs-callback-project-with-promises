module.exports = processTextFile;

// Import fs (File system) Module
const fs = require("fs");

function processTextFile(txtFilePath, fileNamesPath) {
    
    let upperCasePath =  "./upperCaseText.txt";
    let lowerCasePath =  "./lowerCaseText.txt";
    let sortedFilePath = "./sortedText.txt";



    function readFiles(filePath){

        return new Promise( (resolve,reject) =>{      
            
            fs.readFile(filePath, 'utf8', (error1, fileData) => {
                if (error1) {
                    reject("Enable to read the file : - ", error1);
                }
                else {
                    console.log(`\n${filePath.slice(2)} File read successfully.`);
                    resolve(fileData);
                }
            });
        });
    }



    function writeFiles(filePath, fileData){

        return new Promise( (resolve,reject) =>{      
            
            fs.writeFile(filePath , fileData, function (error2) {
                if (error2) {
                    reject("Error writing the file : - ", error2);
                }
                else {
                    console.log(`${filePath.slice(2)} File written successfully.`);
                    resolve(filePath);
                }
            });

        });
    }


    function appendDataToTheFile(filePath, fileData){

        return new Promise( (resolve,reject) =>{      
            
            fs.appendFile(filePath , fileData.slice(2) + "\n", function (error3) {
                if (error3) {
                    reject("Error adding data to the file : - ", error3);
                }
                else {
                    console.log(`Added ${fileData.slice(2)} to the ${filePath.slice(2)}.`);
                    resolve(fileData);
                }
            });

        });
    }

    function deleteFiles(fileNamesPath){


        return new Promise( (resolve,reject) =>{   

            readFiles(fileNamesPath)
            .then( (fileNamesData)=>{
                
                let fileNamesArray = fileNamesData.split("\n");
                fileNamesArray.pop(); 

                function deleteSelectedFile(fileNamesIndex){

                    return new Promise( (resolve,reject) =>{
    
                        if(fileNamesIndex < fileNamesArray.length){
                        fs.unlink(fileNamesArray[fileNamesIndex], (error4) => {
                            if (error4) {
                                reject(error4);
                            }
                            else {
                                console.log(`${fileNamesArray[fileNamesIndex]} is successfully deleted.`);
                                deleteSelectedFile(fileNamesIndex+1)
                                .then( (successMessage)=>{
                                    resolve(successMessage);
                                })
                                .catch((errorWhileDeletingFiles)=>{
                                    reject(errorWhileDeletingFiles);
                                });
                            }
                        });
                        }
                        else if(fileNamesIndex == fileNamesArray.length){
                            fs.writeFile(fileNamesPath ,"", function (error5) {
                                if (error5) {
                                    reject("Error writing the file : - ", error5);
                                }
                                else {
                                    console.log(`${fileNamesArray.length} files deleted.`);
                                    resolve(`\nText of ${fileNamesPath.slice(2)} deleted.`);
                                }
                            });
                        }
                    });
                }
            
                deleteSelectedFile(0)
                .then( (successMessage)=>{
                    resolve(successMessage);
                })
                .catch((errorWhileDeletingFiles)=>{
                    reject(errorWhileDeletingFiles);
                })
            })
            .catch((errorWhileReadingFileNames)=>{
                 reject(errorWhileReadingFileNames);
            });

        });
    }

    // Read a file, write a new file and append the new filename to filenames.txt
    function readWriteAppendFiles(readFilePath, stringFunction, writeFilePath,fileNamesPath){

        return new Promise( (resolve,reject)=>{

            readFiles(readFilePath)
            .then((fileData)=>{
                return writeFiles(writeFilePath, stringFunction(fileData));
            })
            .then( (writeFilePath)=>{
                resolve(appendDataToTheFile(fileNamesPath,writeFilePath));
            })
            .catch((errorInFunction)=>{
                reject(errorInFunction)
            })
        })
    }
    
    // Function to make data upper case
    function upperCase(stringData){
    return stringData.toUpperCase();
    }

    // Function to make data lower case and split into sentences
    function  lowerCaseAndSplit(stringData){
    return stringData.toLowerCase().replaceAll(". ", ".\n");
    }

    // Function to sort data
    function  sortData(stringData){
        return JSON.stringify(stringData.split("\n").sort());
    }


    
readWriteAppendFiles(txtFilePath,upperCase, upperCasePath,fileNamesPath)
.then( (upperCaseFilePath)=>{
    return readWriteAppendFiles(upperCaseFilePath,lowerCaseAndSplit, lowerCasePath,fileNamesPath);
})
.then( (lowerCaseFilePath)=>{
    return readWriteAppendFiles(lowerCaseFilePath,sortData, sortedFilePath,fileNamesPath);
})
.then( ()=>{
    return deleteFiles(fileNamesPath);
})
.then((filesDeleteMessage)=>{
    console.log(filesDeleteMessage);
})
.catch((error)=>{
   console.log(`\nError passed to catch Method : `);
   console.log(error); 
});
    
}