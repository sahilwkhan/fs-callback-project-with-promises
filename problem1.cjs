
module.exports = createAndDeleteJSONFilesWithPromises;


// Import fs (File system) Module
const fs = require("fs");

// Function to create a directory for random JSON files
function createAndDeleteJSONFilesWithPromises(directoryPath, maximumFiles) {

    if (!directoryPath) {
        directoryPath = "./Random_JSON_Files";
    }

    if (!maximumFiles) {
        maximumFiles = 10;
    };

    let numberOfJSONFiles = Math.floor(Math.random() * (maximumFiles - 1));

    function makeFolder(directoryPath){ 

        return new Promise( (resolve, reject) =>{
            fs.mkdir(directoryPath, { recursive: true }, (error1)=>{
                    if(error1){
                        reject(error1);
                    }
                    else{
                        console.log("Successful created a directory named " + directoryPath.slice(directoryPath.lastIndexOf("/") + 1) + "." );
                        resolve(directoryPath);
                    }
                });

            });
    }
    

    // Function for writing random JSON files
    function writeFiles(directoryPath,fileNumber){

        return new Promise( (resolve,reject) =>{        
            
            function createFiles(fileNumber){

                return new Promise( (resolve,reject) =>{

                    if (fileNumber < numberOfJSONFiles) {
                        
                        let fileData = { File_Name: `File_${fileNumber + 1}.json`, Data: `This is a random JSON File no. ${fileNumber} created by asynchronous functions of fs modules.` };

                        fs.writeFile(directoryPath + `/File_${fileNumber + 1}.json`, JSON.stringify(fileData), function (error2) {
                        if (error2) {   
                            reject(error2);
                        }
                        else {
                            console.log(`File no. ${fileNumber + 1} created successfully.`);
                            
                            createFiles(fileNumber+1).then( (successMessage)=>{
                                resolve(successMessage);
                            })
                            .catch((errorWhileCreatingFiles)=>{
                                reject(errorWhileCreatingFiles);
                            });    

                        }
                    });
                    }
                    else if (fileNumber == numberOfJSONFiles){
                        resolve(`${numberOfJSONFiles}`);
                    }

                })
            };
            // Call createfiles function with index 0;
            createFiles(fileNumber).then( (successMessage)=>{
                resolve(successMessage);

            })
            .catch((errorWhileCreatingFiles)=>{
                reject(errorWhileCreatingFiles);
            });

        })
    }
    
    // Function for deleting random JSON files
    function deleteFiles(fileNumber){

        return new Promise( (resolve,reject) =>{     

            function deleteJSONFiles(fileNumber){

                return new Promise( (resolve,reject) =>{
                    
                    if (fileNumber < numberOfJSONFiles) {
                    fs.unlink(directoryPath + `/File_${fileNumber + 1}.json`, function (error3) {
                        if (error3) {
                            reject(error3);
                        }
                        else {
                            console.log(`File no. ${fileNumber + 1} deleted successfully.`);
                           
                            deleteJSONFiles(fileNumber + 1)
                            .then( (successMessage)=>{
                                resolve(successMessage);
                            })
                            .catch((errorWhileDeletingFiles)=>{
                                reject(errorWhileDeletingFiles);
                            });  
                        
                        }
                    });
                    }
                    else if (fileNumber == numberOfJSONFiles){
                    resolve(`${numberOfJSONFiles}`);
                    }
                });
            }

            // Call createfiles function with index 0;
            
            deleteJSONFiles(fileNumber)
            .then( (successMessage)=>{
                resolve(successMessage);
            })
            .catch((errorWhileDeletingFiles)=>{
                reject(errorWhileDeletingFiles);
            });
       
        });
    }

            
    
    makeFolder(directoryPath)
    .then( (pathDirectory) => {
        return writeFiles(pathDirectory,0)
    })
    .then((createMessage)=>{
        console.log("Number of files created : ",createMessage);
        return deleteFiles(0)
    })
    .then((deleteMessage)=>{
        console.log("Number of files deleted : ",deleteMessage);
    })
    .catch((error)=>{
        console.log(error);
    });
        
}